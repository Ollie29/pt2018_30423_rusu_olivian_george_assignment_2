import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.*;
import java.util.concurrent.ArrayBlockingQueue;

public class GUI extends JPanel{
    private int serverNo;
    private int openServers = 1;

    public GUI () {
        setVisible(true);
        setBounds(250,5,680,250);
        setBackground(Color.LIGHT_GRAY);
    }

    public void addServer() {
        openServers++;
    }

    public void setServerNo(int serverNo) {
        this.serverNo = serverNo;
    }

    @Override
    public void paintComponent (Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;
        ArrayBlockingQueue <TaskClass> customers;
        int servedAt, taskNo;

        for (int i=1; i<=serverNo; i++) {

            if (openServers < i) {
                g2.setColor( Color.LIGHT_GRAY);
            }
            else {
                g2.setColor( Color.GREEN);
            }

            g2.fill(new Rectangle2D.Double(30+(i-1)*50,5,30,20));
            g2.setColor( Color.black );
            g2.drawString(""+i,30+(i-1)*50+5, 18);
        }

        for (int i=1; i<=openServers; i++) {
            try {
                ServerClass c = Simulator.servs[i];
                if(c!=null) {
                    customers = c.getTasks();
                    int j = 0;
                    for (TaskClass customer : customers) {

                        taskNo = customer.getId();
                        servedAt = customer.getServedAt();

                        g2.setColor(Color.RED);
                        g2.fill(new Ellipse2D.Double(30 + (servedAt-1) * 50, 30 + j * 25, 30, 20));

                        g2.setColor(Color.black);
                        g2.drawString("" + taskNo, 30 + (servedAt-1) * 50 + 7, 30 + j * 25 + 15);

                        j++;
                    }
                }
            }
            catch(NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

}
