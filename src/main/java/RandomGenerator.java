import java.util.Random;

public class RandomGenerator {

    Random rand = new Random();

    public Integer generateTime(Integer n){
        Integer time = rand.nextInt(n*60) + 1;
        return time;
    }
}
