import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Main {

    private static int serverNo, minTime, maxTime, servTime, upTime, downTime;
    private static GUI graphics;

    public static void main(String[] args) {

        final JPanel panel = new JPanel();
        JFrame frame = new JFrame("HW2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JTextField upTimeField=new JTextField(30);
        final JTextField downTimeField=new JTextField(30);
        final JTextField serverNoField= new JTextField(30);
        final JTextField minTimeField=new JTextField(30);
        final JTextField maxTimeField= new JTextField(30);
        final JTextField servTimeField=new JTextField(30);
        final JButton start=new JButton("START");
        JScrollPane scroller;
        final JTextArea printout;

        printout = new JTextArea();
        printout.setEditable(false);
        printout.setLineWrap(true);
        printout.requestFocus();

        scroller = new JScrollPane(printout);
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);


        JTextArea msg1 = new JTextArea("Store uptime");
        JTextArea msg2 = new JTextArea("Max no of servers (<11)");
        JTextArea msg3 = new JTextArea("Min arrival time[minute(s)]");
        JTextArea msg4 = new JTextArea("Max arrival time[minute(s)]");
        JTextArea msg5 = new JTextArea("Max process time [minute(s)]");

        panel.setLayout(null);
        panel.add(msg1);
        panel.add(upTimeField);
        panel.add(downTimeField);
        panel.add(msg2);
        panel.add(serverNoField);
        panel.add(msg3);
        panel.add(minTimeField);
        panel.add(msg4);
        panel.add(maxTimeField);
        panel.add(msg5);
        panel.add(servTimeField);
        panel.add(start);
        panel.add(scroller);

        msg1.setBounds(4, 4, 169, 22);
        msg1.setBackground(Color.lightGray);
        msg1.setEditable(false);
        upTimeField.setBounds(4, 28, 28, 24);
        downTimeField.setBounds(39, 28, 28, 24);
        msg2.setBounds(4, 58, 169, 19);
        msg2.setBackground(Color.lightGray);
        msg2.setEditable(false);
        serverNoField.setBounds(5, 85, 60, 25);
        msg3.setBounds(5, 115, 207, 20);
        msg3.setBackground(Color.lightGray);
        msg3.setEditable(false);
        minTimeField.setBounds(5, 140, 60, 25);
        msg4.setBounds(5, 170, 208, 20);
        msg4.setBackground(Color.lightGray);
        msg4.setEditable(false);
        maxTimeField.setBounds(5, 195, 60, 25);
        msg5.setBounds(5, 225, 208, 20);
        msg5.setBackground(Color.lightGray);
        msg5.setEditable(false);
        servTimeField.setBounds(5, 250, 60, 25);
        start.setBounds(45, 320, 150, 40);
        start.setBackground(Color.RED);
        start.setForeground(Color.BLACK);
        scroller.setBounds(260, 285, 685, 250);
        printout.setBounds(260, 285, 685, 250);
        printout.setBackground(Color.WHITE);
        printout.setForeground(Color.BLACK);

        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                start.setVisible(false);
                String parse1 = upTimeField.getText();
                String parse2 = serverNoField.getText();
                String parse3 = minTimeField.getText();
                String parse4 = maxTimeField.getText();
                String parse5 = servTimeField.getText();
                String parse6 = downTimeField.getText();

                upTime = Integer.parseInt(parse1);
                serverNo = Integer.parseInt(parse2);
                minTime = Integer.parseInt(parse3);
                maxTime = Integer.parseInt(parse4);
                servTime = Integer.parseInt(parse5);
                downTime = Integer.parseInt(parse6);
                graphics = new GUI();
                panel.add(graphics);
                graphics.setServerNo(serverNo);

                Random rand = new Random();
                if(serverNo<=10){
                    Simulator sim  = new Simulator(serverNo,upTime,downTime,maxTime,minTime,servTime,graphics,printout);
                    sim.start();////
                }
            }
        });

        frame.setSize(1080, 710);
        frame.getContentPane();
        frame.setVisible(true);
        frame.setResizable(false);
        frame.add(panel);



    }
}

