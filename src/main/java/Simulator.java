
import java.util.concurrent.ArrayBlockingQueue;
import javax.swing.*;

public class Simulator extends Thread {

    protected static ServerClass[] servs;
    private int openQueues;
    private int h, m, s;
    private int[] waiting;  // total waiting time for each task before being served, in each queue. total task waiting time/queue
    private int[] beginServe = new int[10];
    private int[] taskWait = new int[10];
    private int[] servServed = new int[10];
    private int[] totalTasks = new int[10]; // total number of tasks passed through queues
    private int totalQueues;
    private JTextArea printout;
    private int oraIn;
    private GUI graphics;
    private int oraOut, maxTime, minTime, serviceTime;
    private static final int NR_MAX_TASKS = 2;  // if there are more than this number
    // of tasks in a queue and if there are unused queues, then add a new queue

    private RandomGenerator timeGenerator;

    public Simulator(int nrCase, int oraIn, int oraOut, int maxTime, int minTime, int serviceTime, GUI graphics, JTextArea printout) {
        timeGenerator = new RandomGenerator();
        this.totalQueues = nrCase;
        this.oraIn = oraIn;
        this.oraOut = oraOut;
        this.maxTime = maxTime;
        this.minTime = minTime;
        this.serviceTime = serviceTime;
        this.graphics = graphics;
        this.printout = printout;
        servs = new ServerClass[nrCase + 1];

        waiting = new int[nrCase + 1];
        for (int i = 1; i <= nrCase; i++) {
            waiting[i] = 0;
        }

        openQueues = 1;
        servs[1] = new ServerClass(graphics, printout);
        servs[1].start();
    }


    //Computes the index of the queue with the smallest number of tasks

    public int whereToPut() {
        int index = 1, min;

            min = servs[1].queueLength();
            for (int i = 2; i <= openQueues; i++) {
                int length = servs[i].queueLength();
                if (length < min) {
                    min = length;
                    index = i;
                }
            }

        return index;
    }


    //Rule for opening a new server (queue): if there are queues available and
    //not used, and the number of tasks at the smallest queue is greater than
    //NR_MAX_TASKS
    public void addServer() {

            if ((servs[whereToPut()].queueLength() == NR_MAX_TASKS
                    && openQueues < totalQueues)) {
                openQueues++;
                graphics.addServer();
                int minIndex = openQueues;

                printout.append("          Opening server no " + openQueues + "\n");
                graphics.removeAll();
                graphics.updateUI();
                graphics.repaint();
                servs[minIndex] = new ServerClass(graphics, printout);
                servs[minIndex].start(); ////////
            }
    }

    public void convertTime(int t) {
        int ora = oraIn;

        h = ora + t / 3600;
        t = t - (t / 3600) * 3600;
        m = t / 60;
        t = t - (t / 60) * 60;
        s = t;
    }

    @Override
    public void run() {
        int arrivTime, servTime, minIndex;
        int time = 0;

            int i = 1;
            int orain = oraIn;
            int oraout = oraOut;
            int time1 = 0;
            while (time1 < oraout * 3600) {

                arrivTime = timeGenerator.generateTime(maxTime - minTime);
                arrivTime = arrivTime + minTime * 60;   //in seconds
                servTime = timeGenerator.generateTime(serviceTime); // in seconds

                minIndex = whereToPut();

                addServer();

                //averageTime = averageTime + waiting[minIndex];
                int waitTime = 0;

                ArrayBlockingQueue<TaskClass> taskQueue = servs[minIndex].getTasks();

                if (!taskQueue.isEmpty()) {
                    Object[] array = taskQueue.toArray();
                    waitTime += ((TaskClass) array[0]).getWaitTime();
                    for (int j = 1; j < array.length; j++) {
                        waitTime += ((TaskClass) array[j]).getServTime();
                    }
                }

                TaskClass c = new TaskClass(i, minIndex, arrivTime, waitTime, servTime);

                totalTasks[minIndex] = totalTasks[minIndex] + 1;
                taskWait[minIndex] = waiting[minIndex];
                servServed[minIndex] = servServed[minIndex] + servTime;

                waiting[minIndex] = waiting[minIndex] + waitTime;
                time = time + arrivTime;


                servs[minIndex].addTask(c);

                convertTime(time);
                time1 = h * 3600 + m * 60 + s;

                if (beginServe[minIndex] == 0) { //statistic purposes
                    beginServe[minIndex] = time1 - orain * 3600;
                }
                printout.append("Task " + i + " arrived at " + twoDigits(h) + ":" + twoDigits(m) + ":" + twoDigits(s) + "\n");
                i++;
                try {
                    sleep(arrivTime * 10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            printout.append("\n\n Time's up!\n\n");
            // The servers won't receive any new tasks, so inform them
            for (i = 1; i <= openQueues; i++) {
                servs[i].stopRunning();
            }
            // Wait for all servers to finish processing tasks
            for (i = 1; i <= openQueues; i++) {
                try {
                    servs[i].join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            printStatistics();
    }


    //Converts an integer with no more than two digits to a String of two
    //characters, the first being 0 if the integer has a single digit.

    private String twoDigits(int nr) {
        return String.format("%1$02d", nr);
    }

    private void printStatistics() {

        int averageTime=0;
        int taskNo=0;
        for(int val: waiting){
            averageTime+=val;
        }

        for (int i = 1; i <= openQueues; i++) {
            taskNo+=totalTasks[i];
        }

        averageTime = averageTime / taskNo;

        if (averageTime < 0) {
            averageTime = -averageTime;
        }


        printout.append("\nTotal no of tasks: "+taskNo+"\n");
        printout.append("\n\nAverage task wait time is " + averageTime / 60 + " minute(s) and " + averageTime % 60 + " second(s)\n\n");

        for (int i = 1; i <= openQueues; i++) {
            convertTime(beginServe[i]);
            printout.append("Server " + i + " started to serve at " + twoDigits(h)
                    + ":" + twoDigits(m) + ":" + twoDigits(s)
                    + " a no of: " + totalTasks[i] + " tasks" + "\n");
            h = m = s = 0;

            taskWait[i] = taskWait[i] / totalTasks[i];

            printout.append("Server " + i + " served " + "" + twoDigits(servServed[i] / 60)
                    + ":" + twoDigits(servServed[i] % 60) + " and the tasks waited in average "
                    + twoDigits(taskWait[i] / 60) + ":" + twoDigits(taskWait[i] % 60) + "\n");
        }
        printout.append("\n\nEnd of Simulation");
    }
}
