public class TaskClass {

    private int id;
    private int servedAt;
    private int arrivTime;
    private int waitTime;
    private int servTime;

    public TaskClass(int id, int servedAt, int arrivTime, int waitTime, int servTime){
        this.id = id;
        this.servedAt = servedAt;
        this.arrivTime = arrivTime;
        this.waitTime = waitTime;
        this.servTime = servTime;
    }

    public int getId(){
        return this.id;
    }

    public int getServedAt() {
        return servedAt;
    }

    public int getWaitTime(){
        return this.waitTime;
    }

    public int getServTime(){
        return this.servTime;
    }
}
