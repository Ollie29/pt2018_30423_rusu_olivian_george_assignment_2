
import javax.swing.*;
import java.util.concurrent.ArrayBlockingQueue;

public class ServerClass extends Thread {

    private ArrayBlockingQueue<TaskClass> tasks;
    private boolean toBeRunning;
    private GUI graphics;
    private JTextArea printout;
    private final static int MAX_QUEUE_SIZE = 20;

    ServerClass(GUI graphics, JTextArea printout) {
        this.graphics = graphics;
        this.printout = printout;
        tasks = new ArrayBlockingQueue<TaskClass>(MAX_QUEUE_SIZE);
        toBeRunning = true;
    }

    public ArrayBlockingQueue<TaskClass> getTasks() {
        return this.tasks;
    }

    public synchronized void addTask(TaskClass c){
        try {
            tasks.put(c);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        graphics.removeAll();
        graphics.updateUI();
        graphics.repaint();
        notifyAll();
    }

    public synchronized int queueLength() {
        notifyAll();
        return tasks.size();
    }

    public void stopRunning() {
        toBeRunning = false;
    }


    //server one client aka remove one from the queue
    public synchronized TaskClass takeTask() {
        TaskClass c = null;
            while (tasks.isEmpty()) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try{
                c = tasks.take();
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }
            printout.append("Task " + c.getId() + " served at server " + c.getServedAt()
                    + ". Serve time " + c.getServTime() / 60 + " minute(s) and " +
                    c.getServTime() % 60 + " second(s). Wait time " +
                    c.getWaitTime() / 60 + " minute(s) and " + c.getWaitTime() % 60 +
                    " second(s).\n");

       notifyAll();
        return c;
    }

    @Override
    public void run() {
        while (toBeRunning) {
            processNextTask();
        }
        // Server has to stop, if there are more tasks then process them:
        while (!tasks.isEmpty()) {
            processNextTask();
        }
    }

    private void processNextTask() {
        TaskClass t = takeTask();
        if (t == null) {
            return;
        }
        try {
            sleep(t.getServTime() * 100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        graphics.removeAll();
        graphics.updateUI();
        graphics.repaint();
    }
}
